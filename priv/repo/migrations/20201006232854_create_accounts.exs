defmodule Sandbox.Repo.Migrations.CreateAccounts do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :account_number, :string
      add :currency_code, :string
      add :enrollment_id, :string
      add :id, :string
      add :name, :string

      timestamps()
    end

  end
end
