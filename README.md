# Teller API Sandbox

To test the app go to https://teller-api-sandbox.herokuapp.com/

Available urls are:

- https://teller-api-sandbox.herokuapp.com/accounts
- https://teller-api-sandbox.herokuapp.com/accounts/:account_id
- https://teller-api-sandbox.herokuapp.com/accounts/:account_id/transactions
- https://teller-api-sandbox.herokuapp.com/accounts/:account_id/transactions/:id

In order to access the API you must provide a Token using basic http auth (the user portion only)
The the API Token must have the `"test_"` prefix in order to work. Other than
that, any key is fair game: `"test_foo"`, `"test_bar"`, `"test_123"` are all valid
keys and will serve different data that will be generated for the key.

## Time travelling

It is possible to get an account or a list of transactions as if it was at a specific date.
The following request will return the a list of account with their balance as of 2020-10-20:

GET https://teller-api-sandbox.herokuapp.com/accounts?today=2020-10-20

This works with transactions too:

GET https://teller-api-sandbox.herokuapp.com/accounts/:account_id/transactions?today=2020-10-20

Documentation and tests are mainly in `lib/sandbox/accounts.ex`.
Tests are mainly doctests in the `Sandbox.Accounts` module.
