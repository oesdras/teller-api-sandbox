defmodule Sandbox.Accounts do
  @moduledoc """
  This module generates Account and Transaction data using erlang terms as seed.

  A seed may be any erlang term. There is nothing special about the seed itself,
  it is just an argument for a hash that will be used to derive the same data every time.

  The only case when the derived data will be different is when two distinct calls
  are made at distinct days. This is because new transactions are added to the ledger every day.

  This means that if you call the functions of this module today and tomorrow with the same
  seed, there will be additional transactions and the balance will be different, but the
  only difference will be additional transactions that will be added for every day that passed
  since the last call.

  It is possible to pass an option for that sets the last day of the ledger, the `:today` option.
  This way we can simulate the passage of time, as well as make the results of this module
  reproducible and time independent.

  Ex:
      # Gets an account with the balance / transactions as of 2020-10-01.
      Sandbox.Accounts.get_account("foo", "bar", today: ~D[2020-10-08])



  """

  alias __MODULE__
  alias Sandbox.Accounts.{Account, Transaction}
  alias Sandbox.Utils
  import Utils, only: [phash: 1]

  @spec list_accounts(any(), keyword()) :: [%Account{}]
  @doc """
  Returns the list of accounts. The list may have between
  1 and 3 Accounts, depending on the seed.

  All data is generated on the fly based on the seed.
  Any erlang term may be used as a seed.

  ## Examples

      The seed "foo" generates a list with two accounts, the size of the list as well as the Account attributes,
      down to every transaction attribute, are derived from the seed.

      iex> #{Accounts}.list_accounts("foo")
      ...> |> #{Utils}.extract([:id, :account_number, :enrollment_id])
      [
        %{account_number: "23023916", enrollment_id: "test_enr_MTgwNzAzMTA=", id: "test_acc_OTE1OTcyMTc="},
        %{account_number: "47675399", enrollment_id: "test_enr_NTE3NTQ3ODg=", id: "test_acc_MTMzNDQzNDQy"}
      ]

      If we call the function again with the same seed, we get the same result

      iex> #{Accounts}.list_accounts("foo")
      ...> |> #{Utils}.extract([:id, :account_number, :enrollment_id])
      [
        %{account_number: "23023916", enrollment_id: "test_enr_MTgwNzAzMTA=", id: "test_acc_OTE1OTcyMTc="},
        %{account_number: "47675399", enrollment_id: "test_enr_NTE3NTQ3ODg=", id: "test_acc_MTMzNDQzNDQy"}
      ]


      If we use "bar" as a seed, we get a list with a single Account. The Account attributes are totally
      different from the ones generated with the seed "foo"

      iex> #{Accounts}.list_accounts("bar")
      ...> |> #{Utils}.extract([:id, :account_number, :enrollment_id])
      [
        %{account_number: "7552082", enrollment_id: "test_enr_OTM5MzM1MjY=", id: "test_acc_NDA2MDk4NTk="}
      ]

      # Any erlang term may be used as a seed, here we get a list with 3 different accounts:

      iex> #{Accounts}.list_accounts({:foo, 42})
      ...> |> #{Utils}.extract([:id, :account_number, :enrollment_id])
      [
        %{account_number: "82954247", enrollment_id: "test_enr_NzY5MjQ2ODA=", id: "test_acc_NDUxMzc4NDM="},
        %{account_number: "71948077", enrollment_id: "test_enr_MTg4ODYyOTE=", id: "test_acc_ODk0MjUzNTk="},
        %{account_number: "61840543", enrollment_id: "test_enr_MzMyOTUyOTM=", id: "test_acc_NzQ0NTk5NDU="}
      ]


  """
  def list_accounts(seed, opts \\ []) do
    hash = phash(seed)
    total = rem(hash, 3)

    for i <- 0..total do
      Account.generate(hash, [{:index, i} | opts])
    end
  end

  @spec get_account(any(), binary(), keyword()) :: %Account{} | nil
  @doc """
  Returns a single account or nil if it can't find one.

  The same seed should be used both to list accounts or get a
  single account.

  ## Examples

      iex> #{Accounts}.get_account("foo", "test_acc_MTMzNDQzNDQy")
      ...> |> #{Utils}.extract([:id, :account_number, :enrollment_id])
      %{account_number: "47675399", enrollment_id: "test_enr_NTE3NTQ3ODg=", id: "test_acc_MTMzNDQzNDQy"}

      iex> #{Accounts}.get_account("bar", "test_acc_NDA2MDk4NTk=")
      ...> |> #{Utils}.extract([:id, :account_number, :enrollment_id])
      %{account_number: "7552082", enrollment_id: "test_enr_OTM5MzM1MjY=", id: "test_acc_NDA2MDk4NTk="}

      iex> #{Accounts}.get_account("foo", "no existing")
      nil

      # We may also controll the passage of time, here is one account as of 2020-10-01
      iex> #{Accounts}.get_account("bar", "test_acc_NDA2MDk4NTk=", today: ~D[2020-10-01])
      ...> |> #{Utils}.extract([:id, :balances])
      %{id: "test_acc_NDA2MDk4NTk=", balances: %{available: -1371636, ledger: -1371636}}

      # And here is the same account five days later
      iex> #{Accounts}.get_account("bar", "test_acc_NDA2MDk4NTk=", today: ~D[2020-10-06])
      ...> |> #{Utils}.extract([:id, :balances])
      %{balances: %{available: -1438286, ledger: -1438286}, id: "test_acc_NDA2MDk4NTk="}


  """
  def get_account(seed, id, opts \\ []) do
    list_accounts(seed, opts)
    |> Enum.find(fn a -> a.id == id end)
  end

  @spec list_transactions(%Account{} | nil, keyword) :: list(%Transaction{}) | nil
  @doc """
  Returns a list of transactions for a given account.

  Options:

    * `:from_id` Limits the list to transactions that are immediately after the one with the
      provided id
    * `:count` Limits the list to a given number of elements


  If the provided `Account` was retrieved passing a `:today` option, the list of transaction
  will be limited to the date passed to the `:today` option used to fetch the `Account`

  If the account is nil, this function also returns nil

  ## Examples

      # Sets `:today` to `~D[2020-10-20]` when fetching the account, then list the last
      # 4 transactions of the ledger.

      iex> #{Accounts}.get_account("foo", "test_acc_MTMzNDQzNDQy", today: ~D[2020-10-20])
      ...> |> #{Accounts}.list_transactions(count: 4)
      ...> |> #{Utils}.extract([:id, :date, :amount, :running_balance])
      [
        %{
          amount: -2240,
          date: ~D[2020-10-20],
          id: "test_txn_Njk4NDI4MDM=",
          running_balance: 2_418_380
        },
        %{
          amount: -6283,
          date: ~D[2020-10-19],
          id: "test_txn_MjA0MDA4ODU=",
          running_balance: 2_420_620
        },
        %{
          amount: -2876,
          date: ~D[2020-10-18],
          id: "test_txn_Njc2MDE1NDg=",
          running_balance: 2_426_903
        },
        %{
          amount: -7592,
          date: ~D[2020-10-18],
          id: "test_txn_MTU0NzUyNTA=",
          running_balance: 2_429_779
        }
      ]


      # Sets `:today` to `~D[2020-10-20]` when fetching the account, then list transactions
      # that are immediately after the one with `id` equal to `"test_txn_Njc2MDE1NDg="`

      iex> #{Accounts}.get_account("foo", "test_acc_MTMzNDQzNDQy", today: ~D[2020-10-20])
      ...> |> #{Accounts}.list_transactions(from_id: "test_txn_Njc2MDE1NDg=")
      ...> |> #{Utils}.extract([:id, :date, :amount, :running_balance])
      [
        %{
          amount: -2240,
          date: ~D[2020-10-20],
          id: "test_txn_Njk4NDI4MDM=",
          running_balance: 2_418_380
        },
        %{
          amount: -6283,
          date: ~D[2020-10-19],
          id: "test_txn_MjA0MDA4ODU=",
          running_balance: 2_420_620
        }
      ]


  """
  def list_transactions(account, opts \\ [])
  def list_transactions(nil, _), do: nil

  def list_transactions(account, opts) do
    %{ledger: ledger} = account
    from_id = Keyword.get(opts, :from_id)
    count = Keyword.get(opts, :count)

    ledger.transactions
    |> filter_from_id(from_id, [])
    |> limit_to_count(count, 0, [])
  end

  #
  # A recursive function that traverses the list of transactions until it finds the
  # one with the id provided by the :from_id option. If no option was passed, the
  # list is returned as is.
  #
  defp filter_from_id(transactions, nil, _), do: transactions
  defp filter_from_id([], _id, filtered), do: :lists.reverse(filtered)
  defp filter_from_id([%{id: id} | _rest], id, filtered), do: :lists.reverse(filtered)

  defp filter_from_id([current | next], id, filtered) do
    filter_from_id(next, id, [current | filtered])
  end

  #
  # A recursive function that traverses the list of transactions until the number
  # of elements is equal to number provided by the `:count` option. If no option was
  # passed, the list is returned as is.
  #
  defp limit_to_count(transactions, nil, _, _), do: transactions

  defp limit_to_count(transactions, count, step, filtered) when is_binary(count) do
    case Integer.parse(count) do
      :error -> transactions
      {c, _} -> limit_to_count(transactions, c, step, filtered)
    end
  end

  defp limit_to_count([], _, _, filtered), do: :lists.reverse(filtered)
  defp limit_to_count(_txs, count, count, filtered), do: :lists.reverse(filtered)

  defp limit_to_count([current | next], count, step, filtered) do
    limit_to_count(next, count, step + 1, [current | filtered])
  end

  @spec get_transaction(%Account{} | nil, binary()) :: %Transaction{} | nil
  @doc """
  Returns a transaction or nil if it can't find one.

  An account must be provided, if the account is nil, the function also
  returns nil.

  ## Examples

      iex> #{Accounts}.get_account("foo", "test_acc_MTMzNDQzNDQy", today: ~D[2020-10-20])
      ...> |> #{Accounts}.get_transaction("test_txn_MjA0MDA4ODU=")
      ...> |> #{Utils}.extract([:id, :date, :amount, :running_balance])
      %{amount: -6283, date: ~D[2020-10-19], id: "test_txn_MjA0MDA4ODU=", running_balance: 2420620}

      # Returns nil when no account is nil
      iex> #{Accounts}.get_transaction(nil, "test_txn_MjA0MDA4ODU=")
      nil

  """
  def get_transaction(nil, _), do: nil

  def get_transaction(account, id) do
    list_transactions(account)
    |> Enum.find(fn t -> t.id == id end)
  end
end
