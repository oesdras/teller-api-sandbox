defmodule Sandbox.Utils do
  def phash(term) do
    :erlang.phash2(term)
  end

  def generate_id(term, prefix \\ "") do
    suffix =
      phash(term)
      |> to_string()
      |> Base.encode64()

    prefix <> suffix
  end

  def parse_date(nil), do: nil

  def parse_date(term) do
    case Date.from_iso8601(term) do
      {:ok, d} -> d
      _ -> nil
    end
  end

  #
  # Private helper to extract attributes from nested data structures
  # Kind of a poor man's lenses
  #
  @spec extract(any, any, any) :: any
  def extract(term, keys, acc \\ %{})

  def extract(term, keys, acc) when is_list(term),
    do: Enum.map(term, &extract(&1, keys, acc))

  def extract(term, keys, acc) when is_struct(term),
    do: extract(Map.from_struct(term), keys, acc)

  def extract(term, keys, acc) when is_list(keys) do
    Enum.reduce(keys, acc, fn k, a -> extract(term, k, a) end)
  end

  def extract(term, {key, rest}, acc) when is_map(term) do
    case Map.get(term, key) do
      nil -> acc
      val -> Map.put(acc, key, extract(val, rest, %{}))
    end
  end

  def extract(term, key, acc) when is_map(term) do
    case Map.get(term, key) do
      nil -> acc
      val -> Map.put(acc, key, val)
    end
  end
end
