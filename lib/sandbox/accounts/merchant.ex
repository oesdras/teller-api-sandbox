defmodule Sandbox.Accounts.Merchant do
  @names [
    "Uber",
    "Uber Eats",
    "Lyft",
    "Five Guys",
    "In-N-Out Burger",
    "Chick-Fil-A",
    "AMC Metreon",
    "Apple",
    "Amazon",
    "Walmart",
    "Target",
    "Hotel Tonight",
    "Misson Ceviche",
    "The Creamery",
    "Caltrain",
    "Wingstop",
    "Slim Chickens",
    "CVS",
    "Duane Reade",
    "Walgreens",
    "Rooster & Rice",
    "McDonald's",
    "Burger King",
    "KFC",
    "Popeye's",
    "Shake Shack",
    "Lowe's",
    "The Home Depot",
    "Costco",
    "Kroger",
    "iTunes",
    "Spotify",
    "Best Buy",
    "TJ Maxx",
    "Aldi",
    "Dollar General",
    "Macy's",
    "H.E. Butt",
    "Dollar Tree",
    "Verizon Wireless",
    "Sprint PCS",
    "T-Mobile",
    "Kohl's",
    "Starbucks",
    "7-Eleven",
    "AT&T Wireless",
    "Rite Aid",
    "Nordstrom",
    "Ross",
    "Gap",
    "Bed, Bath & Beyond",
    "J.C. Penney",
    "Subway",
    "O'Reilly",
    "Wendy's",
    "Dunkin' Donuts",
    "Petsmart",
    "Dick's Sporting Goods",
    "Sears",
    "Staples",
    "Domino's Pizza",
    "Pizza Hut",
    "Papa John's",
    "IKEA",
    "Office Depot",
    "Foot Locker",
    "Lids",
    "GameStop",
    "Sephora",
    "MAC",
    "Panera",
    "Williams-Sonoma",
    "Saks Fifth Avenue",
    "Chipotle Mexican Grill",
    "Exxon Mobil",
    "Neiman Marcus",
    "Jack In The Box",
    "Sonic",
    "Shell"
  ]

  @count length(@names)

  @spec count :: 79
  def count, do: @count

  #
  # Def one clause per index, from 0 to 78:
  #
  #   def find(0), do: "Uber"
  #   def find(1), do: "Uber Eats"
  #   ...
  #   def find(78), do: "Shell"
  #
  for {name, i} <- Enum.with_index(@names) do
    def find(unquote(i)), do: unquote(name)
  end

  #
  # Fallback when i is out of range
  #
  def find(i) when is_integer(i) do
    i
    |> abs()
    |> rem(@count)
    |> find
  end

  #
  # Fallback when i is not an integer
  #
  def find(i) do
    i |> :erlang.phash2() |> find()
  end
end
