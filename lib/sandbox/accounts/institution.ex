defmodule Sandbox.Accounts.Institution do
  alias __MODULE__

  @derive {Jason.Encoder, only: [:name, :id]}

  defstruct [:name, :id]
  @names ["Chase", "Bank of America", "Wells Fargo", "Citi", "Capital One"]

  @all for name <- @names,
           do: %{
             name: name,
             id: String.downcase(name) |> String.replace(~r/\s/, "_"),
             __struct__: __MODULE__
           }

  @count length(@all)

  @spec all :: [%Institution{}, ...]
  def all, do: @all

  @spec find(binary()) :: %Institution{} | nil
  def find(id), do: Enum.find(@all, fn %{id: iid} -> iid == id end)

  @spec of(any) :: %Institution{}
  def of(account_id) do
    n = :erlang.phash2(account_id)
    index = rem(n, @count)

    Enum.with_index(@all)
    |> Enum.find(fn {_e, i} -> i == index end)
    |> elem(0)
  end
end
