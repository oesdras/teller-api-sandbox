defmodule Sandbox.Accounts.Account do
  alias __MODULE__
  alias Sandbox.Accounts.{Institution, Ledger}

  import Sandbox.Utils, only: [phash: 1, generate_id: 2]

  defstruct [
    :account_number,
    :balances,
    :currency_code,
    :enrollment_id,
    :id,
    :institution,
    :name,
    :routing_numbers,
    :ledger
  ]

  @names {
    "Jimmy Carter",
    "Ronald Reagan",
    "George H. W. Bush",
    "Bill Clinton",
    "George W. Bush",
    "Barack Obama",
    "Donald Trump"
  }

  @count tuple_size(@names)

  @spec generate(integer, Keyword.t()) :: %Account{}
  def generate(seed, opts \\ []) do
    i = Keyword.get(opts, :index, 0)
    name = name_for(seed, i)
    id = id_for(seed, i)
    account_number = account_number_for(id)
    enrollment_id = enrollment_id_for(id)
    institution = Institution.of(id)
    currency_code = "USD"
    routing_numbers = routing_numbers_for(id)

    account = %Account{
      name: name,
      id: id,
      enrollment_id: enrollment_id,
      institution: institution,
      currency_code: currency_code,
      routing_numbers: routing_numbers,
      account_number: account_number
    }

    Ledger.generate(account, opts)
  end

  defp name_for(seed, i) do
    head = rem(seed, @count) + i
    index = rem(head, @count)
    elem(@names, index)
  end

  defp id_for(seed, i) do
    generate_id(seed + i, "test_acc_")
  end

  defp account_number_for(id) do
    salt = "account_number"
    phash([id, salt]) |> to_string()
  end

  defp enrollment_id_for(id) do
    generate_id([id, "enrollment_id"], "test_enr_")
  end

  defp routing_numbers_for(id) do
    salt = "routing_numbers"
    ach = phash([id, salt]) |> to_string()
    wire = phash([ach, salt]) |> to_string()
    %{ach: ach, wire: wire}
  end
end
