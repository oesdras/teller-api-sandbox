defmodule Sandbox.Accounts.Transaction do
  alias __MODULE__
  alias Sandbox.Accounts.{Merchant, Account}

  import Sandbox.Utils, only: [phash: 1, generate_id: 2]

  defstruct [
    :type,
    :running_balance,
    :id,
    :description,
    :date,
    :amount,
    :account_id
  ]

  @min_amount 100
  @max_amount 100_00

  @spec generate(%Account{}, Date.t(), keyword) :: %Transaction{}
  def generate(account, date, opts \\ []) do
    index = Keyword.get(opts, :index, 0)
    id = generate_id([account.id, date, index], "test_txn_")

    %{ledger: balance} = account.balances

    amount = amount_for(id)

    running_balance = balance + amount

    %Transaction{
      id: id,
      amount: amount,
      running_balance: running_balance,
      date: date,
      description: Merchant.find(id),
      type: "card_payment",
      account_id: account.id
    }
  end

  defp amount_for(id) do
    hash = phash([id, "transaction_amount_for"])
    abs_amount = rem(hash, @max_amount) + @min_amount
    abs_amount * -1
  end
end
