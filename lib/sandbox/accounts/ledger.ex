defmodule Sandbox.Accounts.Ledger do
  alias __MODULE__
  alias Sandbox.Accounts.{Transaction, Account}
  defstruct [:opening_balance, :opening_date, :balance, :transactions, :count]

  import Sandbox.Utils, only: [phash: 1]

  # 90 daysbefore 2020-10-08
  @max_epoch ~D[2020-07-10]
  # 120 days before 2020-10-08
  @min_epoch ~D[2020-03-10]

  @opening_dates Date.range(@min_epoch, @max_epoch) |> Enum.with_index()

  @max_opening_balance 5_000_000

  @min_opening_balance 1_500_000

  def generate(account, opts \\ []) do
    opening_date = opening_date_for(account)
    opening_balance = opening_balance_for(account)
    balance = opening_balance

    today = Keyword.get(opts, :today, Date.utc_today())
    today = today || Date.utc_today()

    today =
      case Date.compare(opening_date, today) do
        :lt -> today
        _ -> opening_date
      end

    blankLedger = %Ledger{
      opening_date: opening_date,
      opening_balance: opening_balance,
      balance: balance,
      transactions: [],
      count: 0
    }

    account = %Account{account | ledger: blankLedger}

    Date.range(opening_date, today)
    |> Enum.reduce(account, fn date, acc ->
      transactions_for(acc, date)
    end)
  end

  defp transactions_for(account, date) do
    hash = phash([date, "transactions_for/2"])
    # a number between 0 and 5
    n = rem(hash, 6)

    0..n
    |> Enum.reduce(account, fn _, acc ->
      generate_transaction_for(acc, date)
    end)
  end

  defp generate_transaction_for(account, date) do
    ledger = account.ledger
    transactions = ledger.transactions
    balance = ledger.balance

    account = %Account{
      account
      | balances: %{available: balance, ledger: balance}
    }

    transaction = Transaction.generate(account, date, index: ledger.count)

    new_balance = balance + transaction.amount

    new_count = ledger.count + 1

    new_ledger = %Ledger{
      ledger
      | transactions: [transaction | transactions],
        balance: new_balance,
        count: new_count
    }

    %Account{
      account
      | balances: %{available: new_balance, ledger: new_balance},
        ledger: new_ledger
    }
  end

  defp opening_date_for(%{id: id}) do
    count = length(@opening_dates)
    hash = phash([id, "opening_date"])
    index = rem(hash, count)

    Enum.find(@opening_dates, fn {_, i} -> i == index end)
    |> elem(0)
  end

  defp opening_balance_for(%{id: id}) do
    diff = @max_opening_balance - @min_opening_balance
    hash = phash([id, "opening_balance"])
    amount = rem(hash, diff)
    @min_opening_balance + amount
  end
end
