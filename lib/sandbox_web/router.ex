defmodule SandboxWeb.Router do
  use SandboxWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :auth
  end

  defp auth(conn, _opts) do
    with {"test_" <> _ = token, _pass} <- Plug.BasicAuth.parse_basic_auth(conn) do
      assign(conn, :api_token, token)
    else
      a ->
        IO.inspect(a)
        conn |> Plug.BasicAuth.request_basic_auth() |> halt()
    end
  end

  scope "/", SandboxWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/accounts", SandboxWeb do
    pipe_through :api

    resources "/", AccountController, except: [:new, :edit, :create, :update, :delete] do
      resources "/transactions", TransactionController,
        except: [:new, :edit, :create, :update, :delete]
    end
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: SandboxWeb.Telemetry
    end
  end
end
