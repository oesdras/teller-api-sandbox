defmodule SandboxWeb.TransactionView do
  use SandboxWeb, :view
  alias SandboxWeb.TransactionView

  def render("index.json", %{transactions: transactions}) do
    render_many(transactions, TransactionView, "transaction.json")
  end

  def render("show.json", %{transaction: transaction}) do
    render_one(transaction, TransactionView, "transaction.json")
  end

  def render("404.json", %{account_id: id}) do
    Phoenix.View.render(SandboxWeb.AccountView, "404.json", id: id)
  end

  def render("404.json", %{id: id}) do
    %{
      code: "404",
      message: "could not find a transaction with id = #{id}"
    }
  end

  def render("transaction.json", %{transaction: transaction}) do
    %{
      type: transaction.type,
      running_balance: format_currency(transaction.running_balance),
      id: transaction.id,
      description: transaction.description,
      date: transaction.date,
      amount: format_currency(transaction.amount),
      links: links_for(transaction),
      account_id: transaction.account_id
    }
  end

  defp format_currency(cents) do
    (cents / 100.0) |> to_string()
  end

  defp links_for(%{account_id: account_id, id: id}) do
    endpoint = SandboxWeb.Endpoint

    %{
      self: Routes.account_transaction_url(endpoint, :show, account_id, id, %{}),
      account: Routes.account_url(endpoint, :show, account_id, %{})
    }
  end
end
