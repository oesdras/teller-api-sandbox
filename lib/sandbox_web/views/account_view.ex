defmodule SandboxWeb.AccountView do
  use SandboxWeb, :view
  alias SandboxWeb.AccountView

  def render("index.json", %{accounts: accounts}) do
    render_many(accounts, AccountView, "account.json")
  end

  def render("show.json", %{account: account}) do
    render_one(account, AccountView, "account.json")
  end

  def render("404.json", %{id: id}) do
    %{
      code: "404",
      message: "could not find an account with id = #{id}"
    }
  end

  def render("account.json", %{account: account}) do
    %{
      account_number: account.account_number,
      balances: format_balances(account.balances),
      currency_code: account.currency_code,
      enrollment_id: account.enrollment_id,
      id: account.id,
      institution: account.institution,
      links: links_for(account),
      name: account.name,
      routing_numbers: account.routing_numbers
    }
  end

  defp format_balances(balances) do
    Enum.map(balances, fn {k, v} -> {k, format_currency(v)} end)
    |> Enum.into(%{})
  end

  defp format_currency(cents) do
    (cents / 100.0) |> to_string()
  end

  defp links_for(%{id: id}) do
    endpoint = SandboxWeb.Endpoint

    %{
      account: Routes.account_url(endpoint, :show, id, %{}),
      transactions: Routes.account_transaction_url(endpoint, :index, id, %{})
    }
  end
end
