defmodule SandboxWeb.TransactionController do
  use SandboxWeb, :controller

  alias Sandbox.{Accounts, Utils}

  action_fallback SandboxWeb.FallbackController

  def index(conn, %{"account_id" => account_id} = params) do
    %{api_token: token} = conn.assigns
    from_id = Map.get(params, "from_id")
    count = Map.get(params, "count")
    today = Map.get(params, "today") |> Utils.parse_date()
    opts = [from_id: from_id, count: count, today: today]
    account = Accounts.get_account(token, account_id, opts)

    case Accounts.list_transactions(account, opts) do
      nil ->
        render(conn, "404.json", account_id: account_id)

      transactions ->
        render(conn, "index.json", transactions: transactions, account: account)
    end
  end

  def show(conn, %{"id" => id, "account_id" => account_id} = params) do
    %{api_token: token} = conn.assigns
    today = Map.get(params, "today") |> Utils.parse_date()
    account = Accounts.get_account(token, account_id, today: today)
    transaction = Accounts.get_transaction(account, id)

    case {account, transaction} do
      {nil, _} -> render(conn, "404.json", account_id: account_id)
      {_, nil} -> render(conn, "404.json", id: id)
      {acc, txs} -> render(conn, "show.json", transaction: txs, account: acc)
    end
  end
end
