defmodule SandboxWeb.AccountController do
  use SandboxWeb, :controller

  alias Sandbox.{Accounts, Utils}

  action_fallback SandboxWeb.FallbackController

  def index(conn, params) do
    %{api_token: token} = conn.assigns

    today = Map.get(params, "today") |> Utils.parse_date()
    accounts = Accounts.list_accounts(token, today: today)
    render(conn, "index.json", accounts: accounts)
  end

  def show(conn, %{"id" => id} = params) do
    %{api_token: token} = conn.assigns
    today = Map.get(params, "today") |> Utils.parse_date()

    case Accounts.get_account(token, id, today: today) do
      nil -> render(conn, "404.json", %{id: id})
      account -> render(conn, "show.json", account: account)
    end
  end
end
