defmodule SandboxWeb.AccountControllerTest do
  use SandboxWeb.ConnCase

  describe "index" do
    test "lists all accounts for api token = test_any_token", %{conn: conn} do
      conn =
        put_req_header(
          conn,
          "authorization",
          Plug.BasicAuth.encode_basic_auth("test_any_token", "")
        )

      conn = get(conn, Routes.account_path(conn, :index, %{"today" => "2020-10-08"}))

      assert json_response(conn, 200) == [
               %{
                 "account_number" => "126496152",
                 "balances" => %{"available" => "9138.15", "ledger" => "9138.15"},
                 "currency_code" => "USD",
                 "enrollment_id" => "test_enr_MjkyNjYyNTI=",
                 "id" => "test_acc_NTgxMjgyMDI=",
                 "institution" => %{"id" => "chase", "name" => "Chase"},
                 "links" => %{
                   "account" => "http://localhost:4002/accounts/test_acc_NTgxMjgyMDI%3D",
                   "transactions" =>
                     "http://localhost:4002/accounts/test_acc_NTgxMjgyMDI%3D/transactions"
                 },
                 "name" => "Bill Clinton",
                 "routing_numbers" => %{"ach" => "66645698", "wire" => "48717962"}
               },
               %{
                 "account_number" => "58232742",
                 "balances" => %{"available" => "21099.45", "ledger" => "21099.45"},
                 "currency_code" => "USD",
                 "enrollment_id" => "test_enr_OTU1MTg2MjU=",
                 "id" => "test_acc_MTM1MjA5MjM=",
                 "institution" => %{"id" => "chase", "name" => "Chase"},
                 "links" => %{
                   "account" => "http://localhost:4002/accounts/test_acc_MTM1MjA5MjM%3D",
                   "transactions" =>
                     "http://localhost:4002/accounts/test_acc_MTM1MjA5MjM%3D/transactions"
                 },
                 "name" => "George W. Bush",
                 "routing_numbers" => %{"ach" => "5823069", "wire" => "14174251"}
               }
             ]
    end

    test "lists account with id=test_acc_NTgxMjgyMDI= for api token = test_any_token", %{
      conn: conn
    } do
      conn =
        put_req_header(
          conn,
          "authorization",
          Plug.BasicAuth.encode_basic_auth("test_any_token", "")
        )

      conn =
        get(
          conn,
          Routes.account_path(conn, :show, "test_acc_NTgxMjgyMDI=", %{"today" => "2020-10-08"})
        )

      assert json_response(conn, 200) == %{
               "account_number" => "126496152",
               "balances" => %{"available" => "9138.15", "ledger" => "9138.15"},
               "currency_code" => "USD",
               "enrollment_id" => "test_enr_MjkyNjYyNTI=",
               "id" => "test_acc_NTgxMjgyMDI=",
               "institution" => %{"id" => "chase", "name" => "Chase"},
               "links" => %{
                 "account" => "http://localhost:4002/accounts/test_acc_NTgxMjgyMDI%3D",
                 "transactions" =>
                   "http://localhost:4002/accounts/test_acc_NTgxMjgyMDI%3D/transactions"
               },
               "name" => "Bill Clinton",
               "routing_numbers" => %{"ach" => "66645698", "wire" => "48717962"}
             }
    end
  end
end
